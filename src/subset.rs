pub struct Subset<T>(T, T);

impl<T> core::iter::Iterator for Subset<T>
where
    T: core::ops::BitAnd<Output = T> + core::ops::Sub<Output = T> + num_traits::Zero + Copy,
{
    type Item = T;
    fn next(&mut self) -> Option<Self::Item> {
        self.1 = (self.1 - self.0) & self.0;
        Some(self.1)
    } /*
      fn size_hint(&self) -> (usize, Option<usize>) {
          let size = (1 << self.0.count_ones()) as usize;
          (size, Some(size))
      }*/
}
impl<T> Subset<T> {
    pub fn new(a: T) -> Self
    where
        T: num_traits::Zero,
    {
        Subset(a, num_traits::Zero::zero())
    }
}
//implement double ended iterator
/*
// enumerate all subsets of set d
void enumerateAllSubsets(U64 d) {
   U64 n = 0;
   do {
      doSomeThingWithSubset(n);
      n = (n - d) & d;
   } while ( n );
}
*/
//pdep pext may be better...

//I don't understand rust's choice of definition of iterator
//if you want 'external iterator', it should be something like haskell list
//but rust Iterator is something like
//data [] a = [a] | a : [a]
//which doesn't make sense
//I have to implement 'stop state' for all my iterators manually because of this
//In my opinion, in the strictest sense of 'zero cost' abstraction, all iteration should compile to do while loops, which means
//that iterators should be defined to [a] = a | a : [a]
//
//Even the internal iterator days was better