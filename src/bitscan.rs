///Bitscan iterator
#[repr(transparent)]
#[derive(Debug, Copy, Clone)]
pub struct BitScan<T>(pub T);

pub trait GetZeros {
    //todo
    //return trailing zeros on x86_64,
    //return 63 - leading zeros on ARM
    //1<<(63-x) is optimized by compiler
}

impl<T> core::iter::Iterator for BitScan<T>
where
    T: num_traits::int::PrimInt,
{
    type Item = u32;
    fn next(&mut self) -> Option<Self::Item> {
        if self.0.is_zero() {
            None
        } else {
            let temp = self.0.trailing_zeros();
            self.0 = self.0 ^ (T::one() << (temp as usize));
            Some(temp)
        }
    }
    fn size_hint(&self) -> (usize, Option<usize>) {
        let size = self.0.count_ones() as usize;
        (size, Some(size))
    }
}
impl<T> core::iter::ExactSizeIterator for BitScan<T> where T: num_traits::int::PrimInt {}
impl<T> core::iter::FusedIterator for BitScan<T> where T: num_traits::int::PrimInt {}

impl<T> BitScan<T>
where
    T: num_traits::int::PrimInt,
{
    pub fn into_dense(self) -> impl core::iter::Iterator<Item = usize> {
        (0..core::mem::size_of::<T>() * 8).filter(move |&x| !(self.0 & T::one() << x).is_zero())
    }
    pub fn to_dense(&self) -> impl core::iter::Iterator<Item = usize> + '_ {
        (0..core::mem::size_of::<T>() * 8).filter(move |&x| !(self.0 & T::one() << x).is_zero())
    }
}
